﻿namespace VoSP_1_V_Online_Solo_Public_
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.linkSysinternals = new System.Windows.Forms.LinkLabel();
            this.btnSoloPub = new System.Windows.Forms.Button();
            this.linkAbout = new System.Windows.Forms.LinkLabel();
            this.linkAddPsTools = new System.Windows.Forms.LinkLabel();
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.lbProcess = new System.Windows.Forms.Label();
            this.lbPSsuspend = new System.Windows.Forms.Label();
            this.grpInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // linkSysinternals
            // 
            this.linkSysinternals.AutoSize = true;
            this.linkSysinternals.Location = new System.Drawing.Point(3, 16);
            this.linkSysinternals.Name = "linkSysinternals";
            this.linkSysinternals.Size = new System.Drawing.Size(148, 13);
            this.linkSysinternals.TabIndex = 0;
            this.linkSysinternals.TabStop = true;
            this.linkSysinternals.Text = "Download PsTools (Microsoft)";
            this.linkSysinternals.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkSysinternals_LinkClicked);
            // 
            // btnSoloPub
            // 
            this.btnSoloPub.Location = new System.Drawing.Point(14, 81);
            this.btnSoloPub.Name = "btnSoloPub";
            this.btnSoloPub.Size = new System.Drawing.Size(106, 39);
            this.btnSoloPub.TabIndex = 1;
            this.btnSoloPub.Text = "&GO!!!";
            this.btnSoloPub.UseVisualStyleBackColor = true;
            this.btnSoloPub.Click += new System.EventHandler(this.btnSoloPub_Click);
            // 
            // linkAbout
            // 
            this.linkAbout.AutoSize = true;
            this.linkAbout.Location = new System.Drawing.Point(3, 35);
            this.linkAbout.Name = "linkAbout";
            this.linkAbout.Size = new System.Drawing.Size(35, 13);
            this.linkAbout.TabIndex = 0;
            this.linkAbout.TabStop = true;
            this.linkAbout.Text = "About";
            this.linkAbout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAbout_LinkClicked);
            // 
            // linkAddPsTools
            // 
            this.linkAddPsTools.AutoSize = true;
            this.linkAddPsTools.Location = new System.Drawing.Point(3, 53);
            this.linkAddPsTools.Name = "linkAddPsTools";
            this.linkAddPsTools.Size = new System.Drawing.Size(158, 13);
            this.linkAddPsTools.TabIndex = 0;
            this.linkAddPsTools.TabStop = true;
            this.linkAddPsTools.Text = "How to add PsTools (README)";
            this.linkAddPsTools.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkPsTool_LinkClicked);
            // 
            // grpInfo
            // 
            this.grpInfo.Controls.Add(this.linkAddPsTools);
            this.grpInfo.Controls.Add(this.linkAbout);
            this.grpInfo.Controls.Add(this.linkSysinternals);
            this.grpInfo.Location = new System.Drawing.Point(142, 75);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Size = new System.Drawing.Size(166, 75);
            this.grpInfo.TabIndex = 4;
            this.grpInfo.TabStop = false;
            this.grpInfo.Text = "Info";
            // 
            // lbProcess
            // 
            this.lbProcess.AutoSize = true;
            this.lbProcess.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbProcess.Location = new System.Drawing.Point(12, 22);
            this.lbProcess.Name = "lbProcess";
            this.lbProcess.Size = new System.Drawing.Size(52, 15);
            this.lbProcess.TabIndex = 5;
            this.lbProcess.Text = "STATUS";
            // 
            // lbPSsuspend
            // 
            this.lbPSsuspend.AutoSize = true;
            this.lbPSsuspend.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbPSsuspend.Location = new System.Drawing.Point(12, 44);
            this.lbPSsuspend.Name = "lbPSsuspend";
            this.lbPSsuspend.Size = new System.Drawing.Size(68, 15);
            this.lbPSsuspend.TabIndex = 5;
            this.lbPSsuspend.Text = "PSUSPEND";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 162);
            this.Controls.Add(this.lbPSsuspend);
            this.Controls.Add(this.lbProcess);
            this.Controls.Add(this.grpInfo);
            this.Controls.Add(this.btnSoloPub);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "VoSP ©2020 bitelaserkhalif";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkSysinternals;
        private System.Windows.Forms.Button btnSoloPub;
        private System.Windows.Forms.LinkLabel linkAbout;
        private System.Windows.Forms.LinkLabel linkAddPsTools;
        private System.Windows.Forms.GroupBox grpInfo;
        private System.Windows.Forms.Label lbProcess;
        private System.Windows.Forms.Label lbPSsuspend;
    }
}

