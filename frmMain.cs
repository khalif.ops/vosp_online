﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VoSP_1_V_Online_Solo_Public_
{
    public partial class frmMain : Form
    {
        // to initialize download links ect.
        string sysIntDL = "https://docs.microsoft.com/en-us/sysinternals/downloads/pstools";

        string pssus64 = "PsSuspend64 (64 bits) exe";
        string stage1 = "GTA5.exe -accepteula";
        string stage2 = "-r GTA5.exe -accepteula"; //change notepad to GTA5.exe
        bool noDisturb = false;
        bool pss64 = false; //pssuspend64 existed?

        public frmMain()
        {
            InitializeComponent();
            // debug this MessageBox.Show("This is number 15 guy on the black list","HEY");
            //checking presence of file now:
            existance();
        }
        private void existance()
        {
            if (File.Exists("PSTools/pssuspend64.exe"))
            {
                //MessageBox.Show("OK x64", "PsSuspend status");//status of pssuspend.exe 64 bits.
                pss64 = true; //pssuspend64 is existed
            };
            //checking presence of file now:
            if (pss64 == true) //pssuspend64 is existed
            {
               lbPSsuspend.Text =  pssus64 + " exists";
            }
            else if (pss64 == false) //pssuspend64 missing, 32 exists
            {
                lbPSsuspend.Text = pssus64 + " missing";
            }
            else { };
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.WindowsShutDown) && noDisturb == false)
            {
                Application.Exit();
                //fixed the shutdown problem!
                /*DialogResult result = MessageBox.Show("Do you really want to exit?", "EXIT", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    e.Cancel = true;
                }*/
            }
            else if (noDisturb == true)
            {
                e.Cancel = true;
                lbProcess.Text ="For safety reasons, wait till the app unhanged";
            }
            else
            {
                Application.Exit();
            }
        }


        private void linkSysinternals_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(sysIntDL);//opening download page for sysinternals stuff
        }
        private void linkAbout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmAbout f_about = new frmAbout();
            f_about.ShowDialog();
        }

        private void linkPsTool_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("README.TXT");//opening download page for sysinternals stuff
            }
            catch (Exception)
            {
                lbProcess.Text = " README.TXT is missing! Put in same directory with this app!";
            }
        }


        private void btnSoloPub_Click(object sender, EventArgs e)
        { 
            vspType1();
        }

        public async void vspType1()
        {
            //this is where the fun began
            //pssuspend GTA5.exe to begin
            //delay for 10 seconds
            //pssuspend64 -r GTA5.exe

            
                if (pss64 == false) //pssuspend64 is missing
                {
                    if (File.Exists("PSTools.zip"))//if there's a pstools.zip in same exe directory!
                    {
                    //calling the unzipper
                        noDisturb = true;
                        lbProcess.Text = "PSTools.zip has been detected, will unpack...";
                        
                        string zipPath = @"PSTools.zip";
                        string psToolUnp = "PSTools/";
                        ZipFile.ExtractToDirectory(zipPath, psToolUnp);

                        lbProcess.Text = "PSTools.zip has been unpacked!";
                        //unnecessary cleanups
                        string[] files = Directory.GetFiles(psToolUnp);
                        foreach (string file in files)
                        {
                            if (file != psToolUnp+"pssuspend64.exe")//exclude pssuspend64 from deletion
                            {
                                File.Delete(file);
                                lbProcess.Text = file + " has been deleted!";
                            }
                        }
                        lbProcess.Text = "First time setup 2/2 all done";
                        existance(); 
                        noDisturb = false;
                    }
                    else
                    {
                        DialogResult dlPsSuspend = MessageBox.Show("Download the PsTools from microsoft's website?", "DEPENDENCIES MISSING!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dlPsSuspend == DialogResult.Yes)//code to download psSuspend from sysinternals microsoft
                        {
                            //downloading from https://download.sysinternals.com/files/PSTools.zip
                            noDisturb = true;
                            try
                            {
                                using (var client = new WebClient())
                                {
                                    lbProcess.Text = "Downloading...";
                                    client.DownloadFile("https://download.sysinternals.com/files/PSTools.zip", "PSTools.zip");
                                    lbProcess.Text = "Download completed. First time setup 1/2 done";
                                }
                            }
                            catch (WebException e)
                            {
                                MessageBox.Show("Internet problem! " + e, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            vspType1();
                            noDisturb = false;
                        }
                        else if (dlPsSuspend == DialogResult.No)//code to download psSuspend from sysinternals microsoft
                        {
                            MessageBox.Show("We cannot do anything!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else if (pss64 == true)
                {
                    ProcessStartInfo startPsTool = new ProcessStartInfo("PSTools\\pssuspend64.exe");
                    startPsTool.WindowStyle = ProcessWindowStyle.Minimized;
                    if (noDisturb == true)
                    {
                        lbProcess.Text = "For safety reasons, wait till the app unhanged";
                    }
                    else
                    {
                        lbProcess.Text = "EXECUTING STAGE 1 of SoloPublic Lobby: FREEZING";
                        noDisturb = true;
                        startPsTool.Arguments = stage1;
                        Process.Start(startPsTool);//this is stage 1 we will hang the app.

                        //WAIT FOR 10-15 SECONDS
                        lbProcess.Text = "Waiting for 10 seconds....";
                        await Task.Delay(10000);
                    //
                        lbProcess.Text =  "EXECUTING STAGE 2 of SoloPublic Lobby: UNFREEZING";

                        startPsTool.Arguments = stage2;
                        Process.Start(startPsTool);//this is stage 2 we will unhang the app. 
                        noDisturb = false;
                        lbProcess.Text = "DONE";
                    };
                };
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            lbProcess.Text = "VOSP is started";
        }
    }
}
