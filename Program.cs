﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VoSP_1_V_Online_Solo_Public_
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

            if (args.Length == 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmMain());
            }
            else
            {
                string[] arguments = Environment.GetCommandLineArgs();
                if (arguments[1] == "-silent")
                {
                    if (File.Exists("PSTools/pssuspend64.exe"))
                    {
                        string stage1 = "GTA5.exe -accepteula";
                        string stage2 = "-r GTA5.exe -accepteula"; //change notepad to GTA5.exe

                        ProcessStartInfo startPsTool = new ProcessStartInfo("PSTools\\pssuspend64.exe");
                        startPsTool.WindowStyle = ProcessWindowStyle.Minimized;
                        startPsTool.Arguments = stage1;
                        Process.Start(startPsTool);//this is stage 1 we will hang the app.
                        Task.Delay(10000).GetAwaiter().GetResult();
                        startPsTool.Arguments = stage2;
                        Process.Start(startPsTool);//this is stage 2 we will un hang the app
                        Application.Exit();
                    };
                }
                else
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new frmMain());
                }
            }
        }
        /*
                [DllImport("ntdll.dll", PreserveSig = false)]
                public static extern void NtSuspendProcess(IntPtr processHandle);*/
    }
}
