# VOSP



                (    (     
                )\ ) )\ )  
     (   (     (()/((()/(  
     )\  )\ (   /(_))/(_)) 
     ((_)((_))\ (_)) (_))   
     \ \ / /((_)/ __|| _ \  
      \ V // _ \\__ \|  _/  
       \_/ \___/|___/|_|    
                       


28 - 9 - 2020

LATEST REVISION is V1.2
This tool is made for creating solo public lobby in GTA Online.
The way this works is by suspending application for 10 seconds, and unsuspending it back and also by adding firewall rules.
Usually this done by using resource monitor, but that's too complicated and lags a lot.

Changelog:
Version 1.0: Initial release, fixing PsSuspend EULA problem found during debugging. Screenshot might differ.

Version 1.1: Auto download+extract the PsTools from sysinternals server, implement another method of PsSuspend EULA pop-up, add command line switch (-silent), minor polishing. (NO UPDATE GIVEN TO FIREWALL CONTROL)

Version 1.2: Fixing cannot shutdown (my bad) and redesign. (NO UPDATE GIVEN TO FIREWALL CONTROL)

Also included: VoSP FIREWALL CONTROL. **THIS ONE NEEDS UAC ADMIN PROMPT ** (because it doesn't use netsh but using firewall API). Less recommended, but this is permanent.
if it uses netsh, the prompt is: `[netsh advfirewall firewall add rule name="GTA5_SOLOPUB" program=[insert gamedir to exe] protocol=udp dir=out localport=6670,61455-61458 enable=yes action=block profile=Private,Public,Domain]`

Explanation: Blocking with UDP, outward direction, local port of 6670 + 61455-61458, for all type of domain (Private,Public,Domain)


HOW IT WORKS:
The main app acts as frontend of sysinternal's pssuspend and automates all task, so just single click.
The firewall ones? It uses API (not netsh) to create comparable firewall rules with the one made using netsh. With that being said, it needs admin permissions though.

HOW TO USE (pssuspend ones):
1. Run the app with no command line argument.
2. Click "GO!!!", and when you asked for downloading pstools, hit yes. Just wait until it says first time setup 2/2.
3. Open the GTA V, and enter public lobby.
4. Open this app, and click "GO!!!", and watch as everybody left the session!

NEW METHOD!!!
This app now can use command line argument "-silent" without quot. marks, this will make the app run silently with no user interaction!
Requires you to have pssuspend64 on pstools folder first, or initial setup has to be done.

HOW TO USE (firewall ones):
1. Open this app, and click "Add firewall" to add firewall rule. You can optionally add game directory (recommended).
    - Forgot to add game directory? Just tick with EXE path and click browse GTA5.exe. Point it to current GTA5 installation. After that, just click Apply Changes.
2. Open the GTA V, and enter public lobby.
3. To disable and opening the lobby to **everybody**, open this app, and click "Remove firewall" to delete firewall rule.


NOW ENJOY SPECIAL CARGO DELIVERY WITHOUT GETTING MAULED BY TRYHARDS IN OPPRESSOR MARK2!

DISCLAIMER: I AM NOT AFFILIATED WITH ROCKSTAR GAMES, TAKE 2 INTERACTIVE, AND IT'S ASSOCIATES. THIS APP IS TOTALLY FINE TO USE BECAUSE THIS IS NO WAY MODIFYING GAME DATA IN SAME VEIN OF CHEAT OR MOD MENUS. ALSO THIS APP MADE SOLO PUBLIC LOBBY TRICK MORE EASIER BUT USING SIMILAR ALREADY AVAILABLE TRICK. SAFE (REPLICATABLE WITH CRAPPY PC/BAD INTERNET).

THIS APP IS LICENSED UNDER 3 CLAUSE BSD LICENSE, SEE LICENSE.TXT